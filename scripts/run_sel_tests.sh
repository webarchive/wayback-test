#!/bin/bash

usage() { 
		echo "Usage: run_sel_tests.sh -h <help> -c <the selenium node to run tests on. for example: qa-windows.sf.archive.org> -b <the browser platform to run tests on. valid values are: win7_chrome, win7_firefox, win7_ie10, win7_ie11, mac_mountain_lion_chrome, mac_mountain_lion_firefox, mac_mountain_lion_safari> -t <the hostname to test against. for example: https://wayback.qa-archive-it.org> -p <path to py.test> -d <directory containing tests> -n <build number>"
	exit 1
}

PYTHONPATH=/usr/local/lib/python2.7/dist-packages

#defaults
target_host="https://wayback.qa-archive-it.org"
pytest=/usr/local/bin/py.test
build_number=0
test_name=Unknown_test_name
app_tested=Unknown_app_tested
pytest_test_timeout_seconds=900
sleep_time=30
rerun_id=""
conf_test_location=/1/wayback-test/tests/conftest.py
#end defaults

while getopts "c:b:t:p:d:n:h:s:t:e:a:m:f:i:r:o" opt; do
  case $opt in
    h) help="yes"
    ;;
    c) client_host+=("$OPTARG")
    ;;
    b) browser_platform+=("$OPTARG")
    ;;
    t) target_host="${OPTARG}"
    ;;
    o) snapshot="-o"
    ;;    
    p) pytest="${OPTARG}"
    ;;
    d) tests_directory="${OPTARG}"
    ;;
    n) build_number="${OPTARG}"
    ;;
    s) store_api_url="${OPTARG}"
    ;;
    m) test_name="${OPTARG}"
    ;;
    e) test_id="${OPTARG}"
    ;;
    i) sleep_time="${OPTARG}"
    ;;       
    r) rerun_id="${OPTARG}"
    ;;       
    f) test_script_files+=("$OPTARG")
    ;;        
    a) app_tested="${OPTARG}"
    ;;        
    l) conf_test_location="${OPTARG}"
    ;;        
  esac
done


if [ -n "${help}" ] ; then
	usage
fi

#array defaults
if [ -z "${client_host}" ] ; then
    client_host=("qa-auto-vm-win1.sf.archive.org")
fi

if [ -z "${browser_platform}" ] ; then
    browser_platform=("win7_firefox")
fi
#end array defaults

if [ -z "${test_id}" ] ; then
	test_id=`uuidgen`
fi

#if tests_directory exists, prepend it to test file names
if [ ! -z "${tests_directory}" ] ; then
	for file in "${test_script_files[@]}"; do
		test_script_files[$i]="${tests_directory}/${file}"
		let i++
	done
fi

#initialize parameters specific to browser tests and run the tests. for each client host, run on all the browsers/platforms that were specified
for clienthost in "${client_host[@]}"; do
	for browserplatform in "${browser_platform[@]}"; do

		test_options="--browser_platform ${browserplatform} --clienthost ${clienthost}"
		test_options="${test_options} --target ${target_host} --build_number ${build_number} --test_name ${test_name}"
		test_options="${test_options} --sleep_time ${sleep_time}"
		
		if [ -n "${snapshot}" ] ; then
			test_options="${test_options} --snapshot"
		fi
		
		#first check if tests have been disabled if api url specified
		if [ -n "${store_api_url}" ] ; then
			curl -ssl3 -k -s ${store_api_url}/testresultstore/api/json/gettestsenabled |fgrep "\"enabled\":0" >>/dev/null
			
			if [ $? -eq 0 ]; then
				echo "Tests have been disabled for api url at "${store_api_url}/testresultstore/api/json/gettestsenabled

				#log that the test was not run				
				curl -ssl3 -k --data "clientTested=${browserplatform}&testName=${test_name}&detailMessage=Test%20Not%20Run%20Because%20Tests%20Have%20Been%20Disabled&buildTested=${build_number}&externalTestRunId=${test_id}&applicationTested=${app_tested}&testResults=Test%20Not%20Run&clientHost=${clienthost}" ${store_api_url}/testresultstore/api/form/posttestresult
				 
				continue		
			fi
		fi				

		if [ -n ${test_script_files} ]; then
			echo $pytest ${conf_test_location} $test_script_files $test_options --timeout=${pytest_test_timeout_seconds}
		
			start_time=`date +%Y-%m-%dT%H:%M:%S`
			testoutput=`$pytest ${conf_test_location} $test_script_files $test_options --timeout=${pytest_test_timeout_seconds}`
			end_time=`date +%Y-%m-%dT%H:%M:%S`
		else
			echo $pytest ${conf_test_location} $tests_directory $test_options --timeout=${pytest_test_timeout_seconds}
		
			start_time=`date +%Y-%m-%dT%H:%M:%S`
			testoutput=`$pytest ${conf_test_location} $tests_directory $test_options --timeout=${pytest_test_timeout_seconds}`
			end_time=`date +%Y-%m-%dT%H:%M:%S`
		fi

		echo $testoutput |fgrep "failed" >>/dev/null

		if [ $? -eq 0 ]; then
        		#test failed!
        		test_result="Failed"
   		else
    			echo $testoutput |fgrep -i "error" >>/dev/null
    	
    			if [ $? -eq 0 ]; then
            			#errors running test (maybe network timeout?)
            			test_result="Test Not Run"
    			else            
            			#test passed!
            			test_result="Passed"
        		fi
		fi

		if [ -n "${store_api_url}" ] ; then
			testoutputenc=`perl -se 'use URI::Escape; print uri_escape($perltestoutput);' -- -perltestoutput="${testoutput}"`

			if [ -n ${test_script_files} ]; then
				echo "curl -ssl3 -k --data clientTested=${browserplatform}&testName=${test_name}&testRunEndTime=${end_time}&detailMessage=${testoutputenc}&buildTested=${build_number}&externalTestRunId=${test_id}&applicationTested=${app_tested}&testRunStartTime=${start_time}&testResults=${test_result}&clientHost=${clienthost}&testFiles=${test_script_files}&id=${rerun_id} ${store_api_url}/testresultstore/api/form/posttestresult"

				curl -k -ssl3 --data "clientTested=${browserplatform}&testName=${test_name}&testRunEndTime=${end_time}&detailMessage=${testoutputenc}&buildTested=${build_number}&externalTestRunId=${test_id}&applicationTested=${app_tested}&testRunStartTime=${start_time}&testResults=${test_result}&clientHost=${clienthost}&testFiles=${test_script_files}&id=${rerun_id}" ${store_api_url}/testresultstore/api/form/posttestresult						
			else
				echo "curl -ssl3 -k --data clientTested=${browserplatform}&testName=${test_name}&testRunEndTime=${end_time}&detailMessage=${testoutputenc}&buildTested=${build_number}&externalTestRunId=${test_id}&applicationTested=${app_tested}&testRunStartTime=${start_time}&testResults=${test_result}&clientHost=${clienthost}&testFiles=${$tests_directory}&id=${rerun_id} ${store_api_url}/testresultstore/api/form/posttestresult"

				curl -ssl3 -k --data "clientTested=${browserplatform}&testName=${test_name}&testRunEndTime=${end_time}&detailMessage=${testoutputenc}&buildTested=${build_number}&externalTestRunId=${test_id}&applicationTested=${app_tested}&testRunStartTime=${start_time}&testResults=${test_result}&clientHost=${clienthost}&testFiles=${$tests_directory}&id=${rerun_id}" ${store_api_url}/testresultstore/api/form/posttestresult						
			fi
			
			echo $testoutput		
		else
			echo $testoutput
		fi	
	done
done
