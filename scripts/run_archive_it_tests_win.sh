#!/bin/bash
#run regression tests for AIT wayback

if [ $# != 5 ] && [ $# != 6 ];
then
	echo "$0: usage: run_archive_it_tests_win.sh <application name> <wayback server name>  <latestbuildnumber> <test_id> <test_directory> <snapshot>"
	exit 1
fi

app_name=$1
target_wayback_server=$2
lastestbuildnumber=$3
test_id=$4
test_directory=$5
snapshot=$6

#test on win7
/1/wayback-test/scripts/run_sel_tests.sh -n $lastestbuildnumber -b win7_firefox -b win7_chrome -t ${target_wayback_server} -c qa-auto-vm-win1.sf.archive.org -s "http://wbgrp-tmphdp09.us.archive.org:8090" -m wayback_regression_facebook_scroll_test -a "${app_name}" -e $test_id -f test_archive_it_facebook_scroll.py -d $test_directory $snapshot

#ie is special
/1/wayback-test/scripts/run_sel_tests.sh -n $lastestbuildnumber -b win7_ie11 -t ${target_wayback_server} -c qa-auto-vm-win1.sf.archive.org -s "http://wbgrp-tmphdp09.us.archive.org:8090" -m wayback_regression_facebook_scroll_test -a "${app_name}" -e $test_id -f test_archive_it_facebook_scroll.py -d $test_directory $snapshot

/1/wayback-test/scripts/run_sel_tests.sh -n $lastestbuildnumber -b win7_firefox -b win7_chrome -b win7_ie11 -t ${target_wayback_server} -c qa-auto-vm-win1.sf.archive.org -s "http://wbgrp-tmphdp09.us.archive.org:8090" -m wayback_regression_context_flag_test -a "${app_name}" -e $test_id -f test_archive_it_ari_4184.py  -d $test_directory $snapshot

/1/wayback-test/scripts/run_sel_tests.sh -n $lastestbuildnumber -b win7_firefox -b win7_chrome -b win7_ie11 -t ${target_wayback_server} -c qa-auto-vm-win1.sf.archive.org -s "http://wbgrp-tmphdp09.us.archive.org:8090" -m wayback_regression_twitter_scroll_test -a "${app_name}" -e $test_id -f test_archive_it_twitter_scroll.py  -d $test_directory $snapshot

#no ie test because ie uses flash
/1/wayback-test/scripts/run_sel_tests.sh -n $lastestbuildnumber -b win7_chrome -b win7_firefox -t ${target_wayback_server} -c qa-auto-vm-win1.sf.archive.org -s "http://wbgrp-tmphdp09.us.archive.org:8090" -m wayback_regression_video_test -a "${app_name}" -e $test_id -f test_archive_it_wayback_video_regression.py -d $test_directory $snapshot

/1/wayback-test/scripts/run_sel_tests.sh -n $lastestbuildnumber -b win7_firefox -b win7_chrome -b win7_ie11 -t ${target_wayback_server} -c qa-auto-vm-win1.sf.archive.org -s "http://wbgrp-tmphdp09.us.archive.org:8090" -m test_archive_it_ari_4380-issue-3 -a "${app_name}" -e $test_id -f test_archive_it_ari_4380-issue-3.py -d $test_directory $snapshot

/1/wayback-test/scripts/run_sel_tests.sh -n $lastestbuildnumber -b win7_firefox -b win7_chrome -b win7_ie11 -t ${target_wayback_server} -c qa-auto-vm-win1.sf.archive.org -s "http://wbgrp-tmphdp09.us.archive.org:8090" -m test_archive_it_ari_4392 -a "${app_name}" -e $test_id -f test_archive_it_ari_4392.py -d $test_directory $snapshot

/1/wayback-test/scripts/run_sel_tests.sh -n $lastestbuildnumber -b win7_firefox -b win7_chrome -b win7_ie11 -t ${target_wayback_server} -c qa-auto-vm-win1.sf.archive.org -s "http://wbgrp-tmphdp09.us.archive.org:8090" -m test_archive_it_ari_4393 -a "${app_name}" -e $test_id -f test_archive_it_ari_4393.py -d $test_directory $snapshot

/1/wayback-test/scripts/run_sel_tests.sh -n $lastestbuildnumber -b win7_firefox -b win7_chrome -b win7_ie11 -t ${target_wayback_server} -c qa-auto-vm-win1.sf.archive.org -s "http://wbgrp-tmphdp09.us.archive.org:8090" -m test_archive_it_ari_4477 -a "${app_name}" -e $test_id -f test_archive_it_ari_4477.py -d $test_directory $snapshot

/1/wayback-test/scripts/run_sel_tests.sh -n $lastestbuildnumber -b win7_firefox -b win7_chrome -b win7_ie11 -t ${target_wayback_server} -c qa-auto-vm-win1.sf.archive.org -s "http://wbgrp-tmphdp09.us.archive.org:8090" -m test_archive_it_ari_4243 -a "${app_name}" -e $test_id -f test_archive_it_ari_4243.py -d $test_directory $snapshot

/1/wayback-test/scripts/run_sel_tests.sh -n $lastestbuildnumber -b win7_firefox -b win7_chrome -b win7_ie11 -t ${target_wayback_server} -c qa-auto-vm-win1.sf.archive.org -s "http://wbgrp-tmphdp09.us.archive.org:8090" -m test_archive_it_aitfive_446_divergent -a "${app_name}" -e $test_id -f test_archive_it_aitfive_446_divergent.py -d $test_directory $snapshot

#no test because video assets used by this site for ie and firefox weren't captured
/1/wayback-test/scripts/run_sel_tests.sh -n $lastestbuildnumber -b win7_chrome -t ${target_wayback_server} -c qa-auto-vm-win1.sf.archive.org -s "http://wbgrp-tmphdp09.us.archive.org:8090" -m test_archive_it_aitfive_446_whileyoung -a "${app_name}" -e $test_id -f test_archive_it_aitfive_446_whileyoung.py -d $test_directory $snapshot

/1/wayback-test/scripts/run_sel_tests.sh -n $lastestbuildnumber -b win7_firefox -b win7_chrome -b win7_ie11 -t ${target_wayback_server} -c qa-auto-vm-win1.sf.archive.org -s "http://wbgrp-tmphdp09.us.archive.org:8090" -m test_archive_it_ari_4366.py -a "${app_name}" -e $test_id -f test_archive_it_ari_4366.py -d $test_directory $snapshot

/1/wayback-test/scripts/run_sel_tests.sh -n $lastestbuildnumber -b win7_firefox -b win7_chrome -b win7_ie11 -t ${target_wayback_server} -c qa-auto-vm-win1.sf.archive.org -s "http://wbgrp-tmphdp09.us.archive.org:8090" -m test_archive_it_ari_4285.py -a "${app_name}" -e $test_id -f test_archive_it_ari_4285.py -d $test_directory $snapshot

/1/wayback-test/scripts/run_sel_tests.sh -n $lastestbuildnumber -b win7_firefox -b win7_chrome -b win7_ie11 -t ${target_wayback_server} -c qa-auto-vm-win1.sf.archive.org -s "http://wbgrp-tmphdp09.us.archive.org:8090" -m test_archive_it_ari_4168.py -a "${app_name}" -e $test_id -f test_archive_it_ari_4168.py -d $test_directory $snapshot

/1/wayback-test/scripts/run_sel_tests.sh -n $lastestbuildnumber -b win7_firefox -b win7_chrome -b win7_ie11 -t ${target_wayback_server} -c qa-auto-vm-win1.sf.archive.org -s "http://wbgrp-tmphdp09.us.archive.org:8090" -m test_archive_it_aitfive_453.py -a "${app_name}" -e $test_id -f test_archive_it_aitfive_453.py -d $test_directory $snapshot

/1/wayback-test/scripts/run_sel_tests.sh -n $lastestbuildnumber -b win7_firefox -b win7_chrome -b win7_ie11 -t ${target_wayback_server} -c qa-auto-vm-win1.sf.archive.org -s "http://wbgrp-tmphdp09.us.archive.org:8090" -m test_archive_it_ari_4467.py -a "${app_name}" -e $test_id -f test_archive_it_ari_4467.py -d $test_directory $snapshot

/1/wayback-test/scripts/run_sel_tests.sh -n $lastestbuildnumber -b win7_firefox -b win7_chrome -b win7_ie11 -t ${target_wayback_server} -c qa-auto-vm-win1.sf.archive.org -s "http://wbgrp-tmphdp09.us.archive.org:8090" -m test_archive_it_ari_4512.py -a "${app_name}" -e $test_id -f test_archive_it_ari_4512.py -d $test_directory $snapshot

/1/wayback-test/scripts/run_sel_tests.sh -n $lastestbuildnumber -b win7_firefox -b win7_chrome -b win7_ie11 -t ${target_wayback_server} -c qa-auto-vm-win1.sf.archive.org -s "http://wbgrp-tmphdp09.us.archive.org:8090" -m test_archive_it_ari_4691.py -a "${app_name}" -e $test_id -f test_archive_it_ari_4691.py -d $test_directory $snapshot

# tests added spring 2017
/1/wayback-test/scripts/run_sel_tests.sh -n $lastestbuildnumber -b win7_firefox -b win7_chrome -b win7_ie11 -t ${target_wayback_server} -c qa-auto-vm-win1.sf.archive.org -s "http://wbgrp-tmphdp09.us.archive.org:8090" -m test_201701_chubb_twitter -a "${app_name}" -e $test_id -f test_archive_it_201701_chubb_twitter.py -d $test_directory $snapshot
/1/wayback-test/scripts/run_sel_tests.sh -n $lastestbuildnumber -b win7_firefox -b win7_chrome -b win7_ie11 -t ${target_wayback_server} -c qa-auto-vm-win1.sf.archive.org -s "http://wbgrp-tmphdp09.us.archive.org:8090" -m test_201701_chubb_youtube -a "${app_name}" -e $test_id -f test_archive_it_201701_chubb_youtube.py -d $test_directory $snapshot
/1/wayback-test/scripts/run_sel_tests.sh -n $lastestbuildnumber -b win7_firefox -b win7_chrome -b win7_ie11 -t ${target_wayback_server} -c qa-auto-vm-win1.sf.archive.org -s "http://wbgrp-tmphdp09.us.archive.org:8090" -m test_201702_chubb_login -a "${app_name}" -e $test_id -f test_archive_it_201702_chubb_login.py -d $test_directory $snapshot
/1/wayback-test/scripts/run_sel_tests.sh -n $lastestbuildnumber -b win7_firefox -b win7_chrome -b win7_ie11 -t ${target_wayback_server} -c qa-auto-vm-win1.sf.archive.org -s "http://wbgrp-tmphdp09.us.archive.org:8090" -m test_201701_medium -a "${app_name}" -e $test_id -f test_archive_it_201701_medium.py -d $test_directory $snapshot
/1/wayback-test/scripts/run_sel_tests.sh -n $lastestbuildnumber -b win7_firefox -b win7_chrome -b win7_ie11 -t ${target_wayback_server} -c qa-auto-vm-win1.sf.archive.org -s "http://wbgrp-tmphdp09.us.archive.org:8090" -m test_201608_wix -a "${app_name}" -e $test_id -f test_archive_it_201608_wix.py -d $test_directory $snapshot
/1/wayback-test/scripts/run_sel_tests.sh -n $lastestbuildnumber -b win7_firefox -b win7_chrome -b win7_ie11 -t ${target_wayback_server} -c qa-auto-vm-win1.sf.archive.org -s "http://wbgrp-tmphdp09.us.archive.org:8090" -m test_201611_soundcloud -a "${app_name}" -e $test_id -f test_archive_it_201611_soundcloud.py -d $test_directory $snapshot
/1/wayback-test/scripts/run_sel_tests.sh -n $lastestbuildnumber -b win7_firefox -b win7_chrome -b win7_ie11 -t ${target_wayback_server} -c qa-auto-vm-win1.sf.archive.org -s "http://wbgrp-tmphdp09.us.archive.org:8090" -m test_201703_pinterest -a "${app_name}" -e $test_id -f test_archive_it_201703_pinterest.py -d $test_directory $snapshot


#test on win8
/1/wayback-test/scripts/run_sel_tests.sh -n $lastestbuildnumber -b win8_firefox -b win8_chrome -b win8_ie10 -t ${target_wayback_server} -c qa-auto-vm-win2.sf.archive.org -s "http://wbgrp-tmphdp09.us.archive.org:8090" -m wayback_regression_facebook_scroll_test -a "${app_name}" -e $test_id -f test_archive_it_facebook_scroll.py -d $test_directory $snapshot

/1/wayback-test/scripts/run_sel_tests.sh -n $lastestbuildnumber -b win8_firefox -b win8_chrome -b win8_ie10 -t ${target_wayback_server} -c qa-auto-vm-win2.sf.archive.org -s "http://wbgrp-tmphdp09.us.archive.org:8090" -m wayback_regression_context_flag_test -a "${app_name}" -e $test_id -f test_archive_it_ari_4184.py -d $test_directory $snapshot

/1/wayback-test/scripts/run_sel_tests.sh -n $lastestbuildnumber -b win8_firefox -b win8_chrome -b win8_ie10 -t ${target_wayback_server} -c qa-auto-vm-win2.sf.archive.org -s "http://wbgrp-tmphdp09.us.archive.org:8090" -m wayback_regression_twitter_scroll_test -a "${app_name}" -e $test_id -f test_archive_it_twitter_scroll.py -d $test_directory $snapshot

#no ie test because it uses flash
/1/wayback-test/scripts/run_sel_tests.sh -n $lastestbuildnumber -b win8_chrome -b win8_firefox -t ${target_wayback_server} -c qa-auto-vm-win2.sf.archive.org -s "http://wbgrp-tmphdp09.us.archive.org:8090" -m wayback_regression_video_test -a "${app_name}" -e $test_id -f test_archive_it_wayback_video_regression.py -d $test_directory $snapshot

/1/wayback-test/scripts/run_sel_tests.sh -n $lastestbuildnumber -b win8_firefox -b win8_chrome -b win8_ie10 -t ${target_wayback_server} -c qa-auto-vm-win2.sf.archive.org -s "http://wbgrp-tmphdp09.us.archive.org:8090" -m test_archive_it_ari_4380-issue-3 -a "${app_name}" -e $test_id -f test_archive_it_ari_4380-issue-3.py -d $test_directory $snapshot

/1/wayback-test/scripts/run_sel_tests.sh -n $lastestbuildnumber -b win8_firefox -b win8_chrome -b win8_ie10 -t ${target_wayback_server} -c qa-auto-vm-win2.sf.archive.org -s "http://wbgrp-tmphdp09.us.archive.org:8090" -m test_archive_it_ari_4392 -a "${app_name}" -e $test_id -f test_archive_it_ari_4392.py -d $test_directory $snapshot

/1/wayback-test/scripts/run_sel_tests.sh -n $lastestbuildnumber -b win8_firefox -b win8_chrome -b win8_ie10 -t ${target_wayback_server} -c qa-auto-vm-win2.sf.archive.org -s "http://wbgrp-tmphdp09.us.archive.org:8090" -m test_archive_it_ari_4393 -a "${app_name}" -e $test_id -f test_archive_it_ari_4393.py -d $test_directory $snapshot

/1/wayback-test/scripts/run_sel_tests.sh -n $lastestbuildnumber -b win8_firefox -b win8_chrome -b win8_ie10 -t ${target_wayback_server} -c qa-auto-vm-win2.sf.archive.org -s "http://wbgrp-tmphdp09.us.archive.org:8090" -m test_archive_it_ari_4477 -a "${app_name}" -e $test_id -f test_archive_it_ari_4477.py -d $test_directory $snapshot

/1/wayback-test/scripts/run_sel_tests.sh -n $lastestbuildnumber -b win8_firefox -b win8_chrome -b win8_ie10 -t ${target_wayback_server} -c qa-auto-vm-win2.sf.archive.org -s "http://wbgrp-tmphdp09.us.archive.org:8090" -m test_archive_it_ari_4243 -a "${app_name}" -e $test_id -f test_archive_it_ari_4243.py -d $test_directory $snapshot

/1/wayback-test/scripts/run_sel_tests.sh -n $lastestbuildnumber -b win8_firefox -b win8_chrome -b win8_ie10 -t ${target_wayback_server} -c qa-auto-vm-win2.sf.archive.org -s "http://wbgrp-tmphdp09.us.archive.org:8090" -m test_archive_it_aitfive_446_divergent -a "${app_name}" -e $test_id -f test_archive_it_aitfive_446_divergent.py -d $test_directory $snapshot

#no ie and firefox test because assets used by these browsers were not captured
/1/wayback-test/scripts/run_sel_tests.sh -n $lastestbuildnumber -b win8_chrome -t ${target_wayback_server} -c qa-auto-vm-win2.sf.archive.org -s "http://wbgrp-tmphdp09.us.archive.org:8090" -m test_archive_it_aitfive_446_whileyoung -a "${app_name}" -e $test_id -f test_archive_it_aitfive_446_whileyoung.py -d $test_directory $snapshot

/1/wayback-test/scripts/run_sel_tests.sh -n $lastestbuildnumber -b win8_firefox -b win8_chrome -b win8_ie10 -t ${target_wayback_server} -c qa-auto-vm-win2.sf.archive.org -s "http://wbgrp-tmphdp09.us.archive.org:8090" -m test_archive_it_ari_4366 -a "${app_name}" -e $test_id -f test_archive_it_ari_4366.py -d $test_directory $snapshot

/1/wayback-test/scripts/run_sel_tests.sh -n $lastestbuildnumber -b win8_firefox -b win8_chrome -b win8_ie10 -t ${target_wayback_server} -c qa-auto-vm-win2.sf.archive.org -s "http://wbgrp-tmphdp09.us.archive.org:8090" -m test_archive_it_ari_4285 -a "${app_name}" -e $test_id -f test_archive_it_ari_4285.py -d $test_directory $snapshot

/1/wayback-test/scripts/run_sel_tests.sh -n $lastestbuildnumber -b win8_firefox -b win8_chrome -b win8_ie10 -t ${target_wayback_server} -c qa-auto-vm-win2.sf.archive.org -s "http://wbgrp-tmphdp09.us.archive.org:8090" -m test_archive_it_ari_4168 -a "${app_name}" -e $test_id -f test_archive_it_ari_4168.py -d $test_directory $snapshot

/1/wayback-test/scripts/run_sel_tests.sh -n $lastestbuildnumber -b win8_firefox -b win8_chrome -b win8_ie10 -t ${target_wayback_server} -c qa-auto-vm-win2.sf.archive.org -s "http://wbgrp-tmphdp09.us.archive.org:8090" -m test_archive_it_aitfive_453 -a "${app_name}" -e $test_id -f test_archive_it_aitfive_453.py -d $test_directory $snapshot

/1/wayback-test/scripts/run_sel_tests.sh -n $lastestbuildnumber -b win8_firefox -b win8_chrome -b win8_ie10 -t ${target_wayback_server} -c qa-auto-vm-win2.sf.archive.org -s "http://wbgrp-tmphdp09.us.archive.org:8090" -m test_archive_it_ari_4467 -a "${app_name}" -e $test_id -f test_archive_it_ari_4467.py -d $test_directory $snapshot

/1/wayback-test/scripts/run_sel_tests.sh -n $lastestbuildnumber -b win8_firefox -b win8_chrome -b win8_ie10 -t ${target_wayback_server} -c qa-auto-vm-win2.sf.archive.org -s "http://wbgrp-tmphdp09.us.archive.org:8090" -m test_archive_it_ari_4512 -a "${app_name}" -e $test_id -f test_archive_it_ari_4512.py -d $test_directory $snapshot

/1/wayback-test/scripts/run_sel_tests.sh -n $lastestbuildnumber -b win8_firefox -b win8_chrome -b win8_ie10 -t ${target_wayback_server} -c qa-auto-vm-win2.sf.archive.org -s "http://wbgrp-tmphdp09.us.archive.org:8090" -m test_archive_it_ari_4691.py -a "${app_name}" -e $test_id -f test_archive_it_ari_4691.py -d $test_directory $snapshot

#test on win7 with IE9
/1/wayback-test/scripts/run_sel_tests.sh -n $lastestbuildnumber -b win7_ie9 -t ${target_wayback_server} -c qa-auto-vm-win4.sf.archive.org -s "http://wbgrp-tmphdp09.us.archive.org:8090" -m wayback_regression_facebook_scroll_test -a "${app_name}" -e $test_id -f test_archive_it_facebook_scroll.py -d $test_directory $snapshot

/1/wayback-test/scripts/run_sel_tests.sh -n $lastestbuildnumber -b win7_ie9 -t ${target_wayback_server} -c qa-auto-vm-win4.sf.archive.org -s "http://wbgrp-tmphdp09.us.archive.org:8090" -m wayback_regression_context_flag_test -a "${app_name}" -e $test_id -f test_archive_it_ari_4184.py -d $test_directory $snapshot

/1/wayback-test/scripts/run_sel_tests.sh -n $lastestbuildnumber -b win7_ie9 -t ${target_wayback_server} -c qa-auto-vm-win4.sf.archive.org -s "http://wbgrp-tmphdp09.us.archive.org:8090" -m wayback_regression_twitter_scroll_test -a "${app_name}" -e $test_id -f test_archive_it_twitter_scroll.py -d $test_directory $snapshot

#no ie test because ie9 doesn't even do the video replacement and would probably use flash if it did
#/1/wayback-test/scripts/run_sel_tests.sh -n $lastestbuildnumber -b win7_ie9 -t ${target_wayback_server} -c qa-auto-vm-win4.sf.archive.org -s "http://wbgrp-tmphdp09.us.archive.org:8090" -m wayback_regression_video_test -a "${app_name}" -e $test_id -f test_archive_it_wayback_video_regression.py

/1/wayback-test/scripts/run_sel_tests.sh -n $lastestbuildnumber -b win7_ie9 -t ${target_wayback_server} -c qa-auto-vm-win4.sf.archive.org -s "http://wbgrp-tmphdp09.us.archive.org:8090" -m test_archive_it_ari_4380-issue-3 -a "${app_name}" -e $test_id -f test_archive_it_ari_4380-issue-3.py -d $test_directory $snapshot

/1/wayback-test/scripts/run_sel_tests.sh -n $lastestbuildnumber -b win7_ie9 -t ${target_wayback_server} -c qa-auto-vm-win4.sf.archive.org -s "http://wbgrp-tmphdp09.us.archive.org:8090" -m test_archive_it_ari_4392 -a "${app_name}" -e $test_id -f test_archive_it_ari_4392.py -d $test_directory $snapshot

/1/wayback-test/scripts/run_sel_tests.sh -n $lastestbuildnumber -b win7_ie9 -t ${target_wayback_server} -c qa-auto-vm-win4.sf.archive.org -s "http://wbgrp-tmphdp09.us.archive.org:8090" -m test_archive_it_ari_4393 -a "${app_name}" -e $test_id -f test_archive_it_ari_4393.py -d $test_directory $snapshot

#no test cause live doesn't work under ie9
#/1/wayback-test/scripts/run_sel_tests.sh -n $lastestbuildnumber -b win7_ie9 -t ${target_wayback_server} -c qa-auto-vm-win4.sf.archive.org -s "http://wbgrp-tmphdp09.us.archive.org:8090" -m test_archive_it_ari_4477 -a "${app_name}" -e $test_id -f test_archive_it_ari_4477.py

/1/wayback-test/scripts/run_sel_tests.sh -n $lastestbuildnumber -b win7_ie9 -t ${target_wayback_server} -c qa-auto-vm-win4.sf.archive.org -s "http://wbgrp-tmphdp09.us.archive.org:8090" -m test_archive_it_ari_4243 -a "${app_name}" -e $test_id -f test_archive_it_ari_4243.py -d $test_directory $snapshot

#no test cause site needs ie10+
#/1/wayback-test/scripts/run_sel_tests.sh -n $lastestbuildnumber -b win7_ie9 -t ${target_wayback_server} -c qa-auto-vm-win4.sf.archive.org -s "http://wbgrp-tmphdp09.us.archive.org:8090" -m test_archive_it_aitfive_446_divergent -a "${app_name}" -e $test_id -f test_archive_it_aitfive_446_divergent.py

#no test because video assets used by this site for ie weren't captured
#/1/wayback-test/scripts/run_sel_tests.sh -n $lastestbuildnumber -b win7_ie9 -t ${target_wayback_server} -c qa-auto-vm-win4.sf.archive.org -s "http://wbgrp-tmphdp09.us.archive.org:8090" -m test_archive_it_aitfive_446_whileyoung -a "${app_name}" -e $test_id -f test_archive_it_aitfive_446_whileyoung.py

/1/wayback-test/scripts/run_sel_tests.sh -n $lastestbuildnumber -b win7_ie9 -t ${target_wayback_server} -c qa-auto-vm-win4.sf.archive.org -s "http://wbgrp-tmphdp09.us.archive.org:8090" -m test_archive_it_ari_4366.py -a "${app_name}" -e $test_id -f test_archive_it_ari_4366.py -d $test_directory $snapshot

/1/wayback-test/scripts/run_sel_tests.sh -n $lastestbuildnumber -b win7_ie9 -t ${target_wayback_server} -c qa-auto-vm-win4.sf.archive.org -s "http://wbgrp-tmphdp09.us.archive.org:8090" -m test_archive_it_ari_4285.py -a "${app_name}" -e $test_id -f test_archive_it_ari_4285.py -d $test_directory $snapshot

#no video test cause it will do flash
#/1/wayback-test/scripts/run_sel_tests.sh -n $lastestbuildnumber -b win7_ie9 -t ${target_wayback_server} -c qa-auto-vm-win4.sf.archive.org -s "http://wbgrp-tmphdp09.us.archive.org:8090" -m test_archive_it_ari_4168.py -a "${app_name}" -e $test_id -f test_archive_it_ari_4168.py

/1/wayback-test/scripts/run_sel_tests.sh -n $lastestbuildnumber -b win7_ie9 -t ${target_wayback_server} -c qa-auto-vm-win4.sf.archive.org -s "http://wbgrp-tmphdp09.us.archive.org:8090" -m test_archive_it_aitfive_453.py -a "${app_name}" -e $test_id -f test_archive_it_aitfive_453.py -d $test_directory $snapshot

/1/wayback-test/scripts/run_sel_tests.sh -n $lastestbuildnumber -b win7_ie9 -t ${target_wayback_server} -c qa-auto-vm-win4.sf.archive.org -s "http://wbgrp-tmphdp09.us.archive.org:8090" -m test_archive_it_ari_4467.py -a "${app_name}" -e $test_id -f test_archive_it_ari_4467.py -d $test_directory $snapshot

/1/wayback-test/scripts/run_sel_tests.sh -n $lastestbuildnumber -b win7_ie9 -t ${target_wayback_server} -c qa-auto-vm-win4.sf.archive.org -s "http://wbgrp-tmphdp09.us.archive.org:8090" -m test_archive_it_ari_4512.py -a "${app_name}" -e $test_id -f test_archive_it_ari_4512.py -d $test_directory $snapshot

/1/wayback-test/scripts/run_sel_tests.sh -n $lastestbuildnumber -b win7_ie9 -t ${target_wayback_server} -c qa-auto-vm-win4.sf.archive.org -s "http://wbgrp-tmphdp09.us.archive.org:8090" -m test_archive_it_ari_4691.py -a "${app_name}" -e $test_id -f test_archive_it_ari_4691.py -d $test_directory $snapshot
