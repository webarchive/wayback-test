#!/bin/bash
#run regression tests for AIT wayback

if [ $# != 2 ]
then
	echo "$0: usage: run_archive_it_tests.sh <application name> <wayback server name>"
	exit 1
fi

app_name=$1
target_wayback_server=$2
lastestbuildnumber=`cat /1/ci-deploy/last-deployed-build.out`
test_id=`uuidgen`
test_directory="/1/wayback-test/tests/archive_it_org"

#test https only since public site wayback links are https
#no firefox for some video test because firefox uses flash player
#run all tests serially because we don't want to overwhelm the qa auto clients with requests for launching browsers
/1/wayback-test/scripts/run_archive_it_tests_mac.sh "$app_name" "$target_wayback_server" "$lastestbuildnumber" "$test_id" "$test_directory" | tee -a /var/tmp/run_archive_it_tests_mac.out & 

/1/wayback-test/scripts/run_archive_it_tests_win.sh "$app_name" "$target_wayback_server" "$lastestbuildnumber" "$test_id" "$test_directory" "-o" | tee -a /var/tmp/run_archive_it_tests_win.out &