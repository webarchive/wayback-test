wayback-test
============

This is a suite of automated regression tests for Wayback.

Plan is to use py.test as test driver and Selenium with remote
contol server as backend in-browser tests.


Installation
------------

* create and activate virtualenv
* install dependencies with `pip install -r requirements.txt`

Adding Tests
------------
* add Archive-It test file to /1/wayback-test/tests/archive_it_org
* add test command to Mac and Windows run scripts,
run_archive_it_tests_mac.sh and run_archive_it_tests_win.sh,
for each desired browser/os
