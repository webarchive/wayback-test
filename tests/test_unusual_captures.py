"""
Test of replaying captures with anomallies (but shall be `rescued`).
"""
import pytest
import sys
import re

@pytest.mark.parametrize("capture", [
        # in green-000044-20000228185217-952155265-c/green-000047-20000228185217-952182500.arc.gz
        # ARC header has empty Content-type field.
        ('all', '20000304150924', 'http://www.decipher.com/librarysheets/homestyle.css')
        ])
@pytest.mark.wayback("global")
def test_arc_empty_contenttype(wayback, capture):
    res = wayback.get_response(capture)
    assert res.getcode() == 200

