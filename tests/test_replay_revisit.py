"""
Test of replaying revisit records.
"""
import pytest
import re
from urllib import urlencode
from urllib2 import urlopen

@pytest.mark.parametrize("url", [
        #"http://www.eff.org/Activism/E-voting/request_paper.pdf"
        "http://www.eff.org/"
        ])
@pytest.mark.wayback("global")
def test_revisit_header(wayback, url):
    # lookup CDX server for a revisit-original pair
    revisits = {}

    cdxq = "{}/cdx/search?{}".format(
        wayback.target,
        urlencode(dict(url=url, sort='reverse')))

    f = urlopen(cdxq)
    for l in f:
        cdx = l.rstrip().split(' ')
        ts = cdx[1]
        mimetype = cdx[3]
        status = cdx[4]
        hash = cdx[5]

        if mimetype == 'warc/revisit':
            if hash not in revisits:
                revisits[hash] = dict(ts=ts, orig=[])
        else:
            if hash in revisits:
                revisit = revisits[hash]
                revisit['orig'].append(ts)

    # take one sample with original(s)
    samples = [c for c in revisits.values() if len(c['orig']) > 0]
    sample = samples[0]

    # check if X-Archive-Orig-date header field has different values
    # (i.e. header fields are replayed from the revisit record, not
    # the original)
    revisit_ts = sample['ts']
    orig_ts = sample['orig'][0]

    res = wayback.get_response((None, revisit_ts, url))
    revisit_date = res.info().getheader('X-Archive-Orig-date')

    res = wayback.get_response((None, orig_ts, url))
    orig_date = res.info().getheader('X-Archive-Orig-date')

    assert revisit_date != orig_date
