import time

def test_ari_4473(browser_platform_choice, client_host, target, sel, build_number, snapshot_dir, test_name, sleep_time, snapshooter, templateloader, snapshot):
    
    url = target + "/5182/20150721194355/http://youaredope.com/"
    
    with snapshooter.prepare(sel, url, browser_platform_choice, test_name, snapshot):
        sel.get(url)
    
        #wait for load - this is a long loading page
        time.sleep(sleep_time)
    
        template = templateloader.load_template("test_archive_it_ari_4473.js.jinja")
        is_video_ended = sel.execute_script(template.render())
        
        assert is_video_ended == False