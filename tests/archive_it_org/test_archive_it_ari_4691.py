from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

def test_ari_4691(browser_platform_choice, client_host, target, sel, build_number, snapshot_dir, test_name, sleep_time, snapshooter, templateloader, snapshot):
    
    url = target + "/4146/20151221203649/https://www.mormon.am/"

    with snapshooter.prepare(sel, url, browser_platform_choice, test_name, snapshot):    
        sel.get(url)

        banner = WebDriverWait(sel, 180).until(EC.presence_of_element_located((By.CSS_SELECTOR, "#wm-disclaim")))
        assert banner != None