import time
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

def test_ari_4243(browser_platform_choice, client_host, target, sel, build_number, snapshot_dir, test_name, sleep_time, snapshooter, templateloader, snapshot):
    
    url = target + "/5182/20150124001440/http://mortdecaithemovie.com/"

    with snapshooter.prepare(sel, url, browser_platform_choice, test_name, snapshot):    
        sel.get(url)
        sel.maximize_window()

        element = WebDriverWait(sel, 180).until(EC.presence_of_element_located((By.CSS_SELECTOR, ".character-man")))    
        element_size = element.size
        
        assert element_size != None
        
        assert element.size["width"] > 100 and element.size["height"] > 100 
        
        assert element != None
        
        style = element.get_attribute("style")
        
        assert style != None
        
        assert style.index("visibility: visible") >= 0
        
        #click right nav arrow
        element = WebDriverWait(sel, 180).until(EC.presence_of_element_located((By.CSS_SELECTOR, ".slider-right-control")))
        
        assert element != None
        
        element.click()
        
        #find textual content
        element = WebDriverWait(sel, 180).until(EC.presence_of_element_located((By.CSS_SELECTOR, ".according-content-wrapper")))
        
        assert element != None
        
        style = element.get_attribute("style")
        
        assert style != None
        
        assert style.index("visibility: visible") >= 0