import time

# For https://webarchive.jira.com/browse/ARI-4328
def test_video_plays(browser_platform_choice, client_host, target, sel, build_number, snapshot_dir, test_name, sleep_time, snapshooter, templateloader, snapshot):
    url = target + "/5235/20150322193218/https://www.youtube.com/watch?v=aHXt1WEoWiw"

    with snapshooter.prepare(sel, url, browser_platform_choice, test_name, snapshot):
        try:
            sel.implicitly_wait(180)
        except Exception as excp:
            print ("Error: " + str(excp))
            time.sleep(sleep_time)

        sel.get(url)
    
        video_element = sel.find_element_by_css_selector("[id^='_wm_video_embed_']")
        assert video_element != None
        
        template = templateloader.load_template("test_archive_it_wayback_video_regression_play_video.js.jinja")
        sel.execute_script(template.render())        

        time.sleep(sleep_time)
        
        template = templateloader.load_template("test_archive_it_wayback_video_regression_is_video_playing.js.jinja")
        is_video_ended = sel.execute_script(template.render())
        
        assert is_video_ended == False