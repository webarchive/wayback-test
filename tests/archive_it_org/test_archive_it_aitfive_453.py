import time

def test_aitfive_453(browser_platform_choice, client_host, target, sel, build_number, snapshot_dir, test_name, sleep_time, snapshooter, templateloader, snapshot):
    
    url = target + "/5921/20150616021443/http://bukkertillibul.net/Archive.html"

    with snapshooter.prepare(sel, url, browser_platform_choice, test_name, snapshot):
        sel.get(url)
        
        #wait for document to load    
        time.sleep(sleep_time)

        template = templateloader.load_template("test_archive_it_aitfive_453-1.js.jinja")
        test_passed = sel.execute_script(template.render())    

        assert test_passed == True

        #wait for document to load    
        time.sleep(sleep_time)
    
        template = templateloader.load_template("test_archive_it_aitfive_453-2.js.jinja")
        test_passed = sel.execute_script(template.render())    

        assert test_passed == True
    
        #wait for document to load    
        time.sleep(sleep_time)
        
        template = templateloader.load_template("test_archive_it_aitfive_453-3.js.jinja")
        test_passed = sel.execute_script(template.render())    
        
        assert test_passed == True
