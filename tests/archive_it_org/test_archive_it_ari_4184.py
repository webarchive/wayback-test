import re
import time

def test_archive_it_ari_4184(browser_platform_choice, client_host, target, sel, build_number, snapshot_dir, test_name, sleep_time, snapshooter, snapshot):

    cssstring_regex = r"""href="{}/4216/\d+cs_/http://focalpoint\.cas\.msu\.edu/wp-content/themes/arras/css/styles/default\.css""".format(target)
    jsstring_regex = r"""src="{}/4216/\d+js_/http://focalpoint\.cas\.msu\.edu/wp-includes/js/jquery/jquery\.js\?ver=1\.10\.2""".format(target)
    
    url = target + "/4216/20150205182746/http://focalpoint.cas.msu.edu/"
    
    with snapshooter.prepare(sel, url, browser_platform_choice, test_name, snapshot):
        try:
            sel.implicitly_wait(180)
        except Exception as excp:
            print ("Error: " + str(excp))
            time.sleep(sleep_time)
            
        sel.get(url)
    
        #IE sometimes has uppercase while other browsers don't, thus the call to lower()
        assert re.search(cssstring_regex, sel.page_source.lower()) != None
        assert re.search(jsstring_regex, sel.page_source.lower()) != None
