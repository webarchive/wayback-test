import time

def test_ari_4254(browser_platform_choice, client_host, target, sel, build_number, snapshot_dir, test_name, sleep_time, snapshooter, templateloader, snapshot):
    
    url = target + "/5182/20150220172248/http://www.fiftyshadesmovie.com/"
    
    with snapshooter.prepare(sel, url, browser_platform_choice, test_name, snapshot):
        sel.get(url)
        
        #wait for load - this is a long loading page
        time.sleep(60)

        template = templateloader.load_template("test_archive_it_ari_4254-1.js.jinja")
        sel.execute_script(template.render())
        
        template = templateloader.load_template("test_archive_it_ari_4254-2.js.jinja")
        test_passed = sel.execute_script(template.render())
    
        assert test_passed == True