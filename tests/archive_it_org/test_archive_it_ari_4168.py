import time
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

def test_ari_4168(browser_platform_choice, client_host, target, sel, build_number, snapshot_dir, test_name, sleep_time, snapshooter, templateloader, snapshot):
    
    urls = [target + "/3784/20150305002317/https://vimeo.com/105131454", target + "/3784/20150305001351/https://vimeo.com/111782582"]

    for url in urls:
        with snapshooter.prepare(sel, url, browser_platform_choice, test_name, snapshot):
            
            sel.get(url)
    
            #get the video link from wb banner
            video_link = WebDriverWait(sel, 180).until(EC.presence_of_element_located((By.LINK_TEXT, "Videos")))            
            
            assert video_link != None
            
            #ActionChains(sel).move_to_element(video_link).click(video_link).perform()
            video_link.click()
    
            #get the first watch link from the public site search result set
            watch_link = WebDriverWait(sel, 180).until(EC.presence_of_element_located((By.LINK_TEXT, "Watch")))            
            
            #ActionChains(sel).move_to_element(watch_link).click(watch_link).perform()
            watch_link.click()
            
            #get video tag from watch page
            video_element = WebDriverWait(sel, 180).until(EC.presence_of_element_located((By.TAG_NAME, "video")))            
            
            assert video_element != None
            
            #give video time to start autoplaying
            time.sleep(sleep_time)
            
            #confirm video is playing
            template = templateloader.load_template("test_archive_it_ari_4168.js.jinja")
            is_video_paused = sel.execute_script(template.render())        
        
            assert is_video_paused == False