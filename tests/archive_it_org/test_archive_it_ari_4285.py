from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

def test_ari_4285(browser_platform_choice, client_host, target, sel, build_number, snapshot_dir, test_name, sleep_time, snapshooter, templateloader, snapshot):
    
    url = target + "/5422/20150310140845/http://www.nytimes.com/2014/11/27/opinion/nicholas-kristof-bill-cosby-uva-and-rape.html?smprod=nytcore-ipad&smid=nytcore-ipad-share&_r=0"
    
    with snapshooter.prepare(sel, url, browser_platform_choice, test_name, snapshot):
        sel.get(url)

        element_comment_count = WebDriverWait(sel, 180).until(EC.presence_of_element_located((By.CSS_SELECTOR, "#story-meta > button > span > span.count")))    
        assert element_comment_count != None
        assert element_comment_count.text == "321"
        
        element_comment_count_text = WebDriverWait(sel, 180).until(EC.presence_of_element_located((By.CSS_SELECTOR, "#story-meta > button > span > span.units")))    
        assert element_comment_count_text != None
        assert element_comment_count_text.text == "COMMENTS"    
        
        element_first_comment_user_name = WebDriverWait(sel, 180).until(EC.presence_of_element_located((By.CSS_SELECTOR, ".commenter")))    
        
        assert element_first_comment_user_name != None
        assert element_first_comment_user_name.text == "Ainamacar"