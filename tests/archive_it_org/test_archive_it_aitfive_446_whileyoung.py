import time

def test_aitfive_466_whileyoung(browser_platform_choice, client_host, target, sel, build_number, snapshot_dir, test_name, sleep_time, snapshooter, templateloader, snapshot):
    
    url = target + "/5182/20150401214808/http://while-were-young.com"
    
    with snapshooter.prepare(sel, url, browser_platform_choice, test_name, snapshot):
        sel.get(url)
        
        #wait for load - this is a long loading page
        time.sleep(60)
    
        template = templateloader.load_template("test_archive_it_aitfive_466_whileyoung.js.jinja")
        is_video_ended = sel.execute_script(template.render())
        
        assert is_video_ended == False