# -*- coding: utf-8 -*-
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

def test_ari_4152(browser_platform_choice, client_host, target, sel, build_number, snapshot_dir, test_name, sleep_time, snapshooter, templateloader, snapshot):
    
    text_to_check = "Communiqué de presse\nServices partagés Canada lance SAGE 2.0 – La nouvelle technologie à l’appui d’un milieu de travail moderne"

    url = target + "/3935/20150910214014/http://www.spc.gc.ca/index-fra.html"

    with snapshooter.prepare(sel, url, browser_platform_choice, test_name, snapshot):
        sel.get(url)

        p_heading_element = WebDriverWait(sel, 180).until(EC.presence_of_element_located((By.CSS_SELECTOR, "#gcwu-headlines > section > ul > li.list-odd > p:nth-child(1)")))
                                                          
        assert p_heading_element.text.encode("utf-8") == text_to_check