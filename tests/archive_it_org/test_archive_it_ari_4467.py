import time
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

def test_ari_4467(browser_platform_choice, client_host, target, sel, build_number, snapshot_dir, test_name, sleep_time, snapshooter, templateloader, snapshot):
    
    url = target + "/5182/20150615204428/http://www.insidiouschapter3.com/"

    with snapshooter.prepare(sel, url, browser_platform_choice, test_name, snapshot):    
        sel.get(url)
        time.sleep(sleep_time)
    
        template = templateloader.load_template("test_archive_it_click_element.js.jinja")
        sel.execute_script(template.render(selector_of_element_to_click="#dark > div > div > div.side-btn"))
    
        template = templateloader.load_template("test_archive_it_click_element.js.jinja")
        sel.execute_script(template.render(selector_of_element_to_click="#slat-1 > div > div > div.circle-expanded > div.nav-title"))
        
        next_picture_button = WebDriverWait(sel, 180).until(EC.presence_of_element_located((By.CSS_SELECTOR, "#section-1 > div > div.home-content > div.content > div.arrow-right > div")))
        assert next_picture_button != None