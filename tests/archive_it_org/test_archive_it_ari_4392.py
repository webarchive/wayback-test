import time

def test_ari_4392(browser_platform_choice, client_host, target, sel, build_number, snapshot_dir, test_name, sleep_time, snapshooter, templateloader, snapshot):
    
    url = target + "/4544/20140626220911/http://wangechimutu.com/"
    imgurl = target + "/4544/20140626220918cs_/http://wangechimutu.com/wp-content/themes/wangechi_mutu/images/splotch_alpha/splotch_representation_alpha59.png"
    
    with snapshooter.prepare(sel, url, browser_platform_choice, test_name, snapshot):
        sel.get(url)
    
        template = templateloader.load_template("test_archive_it_ari-4392.js.jinja")
        sel.execute_script(template.render(imgurl=imgurl))
        
        #wait for image to load
        time.sleep(sleep_time)
    
        is_bad_image = sel.execute_script(" return window.bad_splotch_img; ")
        assert is_bad_image == False