import time

def test_ari_4392(browser_platform_choice, client_host, target, sel, build_number, snapshot_dir, test_name, sleep_time, snapshooter, templateloader, snapshot):
    
    url = target + "/4387/20140302224842/http://www.moma.org/interactives/exhibitions/2011/fluxus_editions/category_works/fluxyearbox2/"
    
    with snapshooter.prepare(sel, url, browser_platform_choice, test_name, snapshot):
        sel.get(url)

        template = templateloader.load_template("test_archive_it_ari-4393.js.jinja")
        sel.execute_script(template.render())
        
        #wait for image to load
        time.sleep(sleep_time)
    
        isbadimage = sel.execute_script(" return window.bad_img; ")
        assert isbadimage == False