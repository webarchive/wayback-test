import time
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

def test_facebook_scroll(browser_platform_choice, client_host, target, sel, build_number, sleep_time, test_name, snapshooter, snapshot):
    url = target + "/3393/20140606135434/https://www.facebook.com/SmithsonianAPA"

    with snapshooter.prepare(sel, url, browser_platform_choice, test_name, snapshot):
        sel.get(url)
        time.sleep(sleep_time)
    
        #scroll down first time
        sel.execute_script("window.scrollTo(0,document.body.scrollHeight);")
        time.sleep(sleep_time)
        
        first_element = WebDriverWait(sel, 180).until(EC.presence_of_element_located((By.CSS_SELECTOR, "#\\31 76353495874662-1 > div > div._5dro._5drq > div > span")))
        assert first_element != None
        
        #scroll down second time
        sel.execute_script("window.scrollTo(0,document.body.scrollHeight);")
        second_element = WebDriverWait(sel, 180).until(EC.presence_of_element_located((By.CSS_SELECTOR, "#u_1b_3 > div:nth-child(1) > div:nth-child(1) > div:nth-child(3) > a:nth-child(1) > div:nth-child(1) > div:nth-child(1) > img:nth-child(1)")))
        assert second_element != None