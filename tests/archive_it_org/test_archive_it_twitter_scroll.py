import time
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

def test_twitter_scroll(browser_platform_choice, client_host, target, sel, build_number, test_name, sleep_time, snapshooter, templateloader, snapshot):
    url = target + "/4672/20140916171205/https://twitter.com/LaurierArchives"

    with snapshooter.prepare(sel, url, browser_platform_choice, test_name, snapshot):
        sel.get(url)
        
        time.sleep(sleep_time)
        #close initial popup if it opens
        template = templateloader.load_template("test_archive_it_twitter_scroll.js.jinja")
        sel.execute_script(template.render())
    
        #scroll down first time
        sel.execute_script("window.scrollTo(0,document.body.scrollHeight);")
        
        first_element = WebDriverWait(sel, 180).until(EC.presence_of_element_located((By.CSS_SELECTOR, "#stream-item-tweet-503925953194901507 > div > div.ProfileTweet-contents > p")))
        assert first_element != None
        
        #scroll down second time
        sel.execute_script("window.scrollTo(0,document.body.scrollHeight);")
        
        #wait 20 seconds so all scroll content has been loaded before scrolling again. otherwise, second scroll won't work
        second_element = WebDriverWait(sel, 180).until(EC.presence_of_element_located((By.CSS_SELECTOR, "#stream-item-tweet-492315301355466752 > div > div.ProfileTweet-contents > div.js-tweet-details-fixer.tweet-details-fixer > div.TwitterPhoto.js-media-container > div > div > a > img")))
        assert second_element != None