import time
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

def test_201701_chubb_twitter(browser_platform_choice, client_host, target, sel, build_number, snapshot_dir, sleep_time, test_name, snapshooter, templateloader, snapshot):
    url = target + "/7125/20170124161526/https://twitter.com/chubb"

    with snapshooter.prepare(sel, url, browser_platform_choice, test_name, snapshot):
        sel.get(url)

        time.sleep(sleep_time)
        #close initial popup if it opens
        template = templateloader.load_template("test_archive_it_twitter_scroll.js.jinja")
        sel.execute_script(template.render())

        #scroll down first time
        sel.execute_script("window.scrollTo(0,document.body.scrollHeight);")

        first_element = WebDriverWait(sel, 180).until(EC.presence_of_element_located((By.CSS_SELECTOR, "li#stream-item-tweet-809493810812256256")))
        assert first_element != None

        #scroll down second time
        sel.execute_script("window.scrollTo(0,document.body.scrollHeight);")

        #wait 20 seconds so all scroll content has been loaded before scrolling again. otherwise, second scroll won't work
        second_element = WebDriverWait(sel, 180).until(EC.presence_of_element_located((By.CSS_SELECTOR, "li#stream-item-tweet-790595907330859008")))
        assert second_element != None
