import time
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

# For https://webarchive.jira.com/browse/ARI-4366
def test_ari_4366(browser_platform_choice, client_host, target, sel, build_number, snapshot_dir, test_name, sleep_time, snapshooter, templateloader, snapshot):
    url = target + "/5182/20150531215006/http://www.seedsoftimemovie.com/"

    with snapshooter.prepare(sel, url, browser_platform_choice, test_name, snapshot):
        sel.get(url)

        video_element = WebDriverWait(sel, 180).until(EC.presence_of_element_located((By.CSS_SELECTOR, "[id^='_wm_video_embed_']")))
        assert video_element != None
        
        template = templateloader.load_template("test_archive_it_wayback_video_regression_play_video.js.jinja")
        sel.execute_script(template.render())        
        
        time.sleep(sleep_time);
        
        template = templateloader.load_template("test_archive_it_wayback_video_regression_is_video_playing.js.jinja")
        is_video_ended = sel.execute_script(template.render())
                
        #video should still be playing if all is well!
        assert is_video_ended == False