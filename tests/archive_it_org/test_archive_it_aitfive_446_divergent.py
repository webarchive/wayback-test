from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

def test_aitfive_466_divergent(browser_platform_choice, client_host, target, sel, build_number, snapshot_dir, test_name, sleep_time, snapshooter, templateloader, snapshot):
    
    url = target + "/5182/20150805005615/http://www.thedivergentseries.com/"
    
    with snapshooter.prepare(sel, url, browser_platform_choice, test_name, snapshot):
        sel.get(url)
        
        try:    
            WebDriverWait(sel, 180).until(EC.text_to_be_present_in_element((By.CSS_SELECTOR, ".homeNavTitle"), "SHATTER REALITY"))
        except Exception as excp:
            print ("Error: " + str(excp))
            assert True == False
            
        assert True == True