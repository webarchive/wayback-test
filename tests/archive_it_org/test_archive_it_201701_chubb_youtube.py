import time
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

def test_201701_chubb_youtube(browser_platform_choice, client_host, target, sel, build_number, snapshot_dir, sleep_time, test_name, snapshooter, templateloader, snapshot):
    
    url = target + "/6884/20170126150757/https://www.youtube.com/channel/UCyzrh3Y3G3r0NfiAjUeXNxA/videos"

    with snapshooter.prepare(sel, url, browser_platform_choice, test_name, snapshot):
        sel.get(url)

        time.sleep(sleep_time)
        #scroll down once
        sel.execute_script("window.scrollTo(0,document.body.scrollHeight);")

        element = WebDriverWait(sel, 180).until(EC.presence_of_element_located((By.CSS_SELECTOR, "li.channels-content-item")))
        assert element != None
