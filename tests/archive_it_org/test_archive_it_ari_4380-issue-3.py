from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

def test_ari_4380_issue_3(browser_platform_choice, client_host, target, sel, build_number, snapshot_dir, sleep_time, test_name, snapshooter, templateloader, snapshot):
    
    url = target + "/4269/20150506142456/http://www.frick.org/"
    
    with snapshooter.prepare(sel, url, browser_platform_choice, test_name, snapshot):
        sel.get(url)
    
        element = WebDriverWait(sel, 180).until(EC.presence_of_element_located((By.CSS_SELECTOR, ".flexslider-processed")))
        assert element != None