from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
import time


def test_ari_4364(browser_platform_choice, client_host, target, sel, build_number, snapshot_dir, test_name, sleep_time, snapshooter, templateloader, snapshot):
    
    url = target + "/4672/20150527152505/https://www.youtube.com/watch?v=5884_Q4SAzg"
    
    with snapshooter.prepare(sel, url, browser_platform_choice, test_name, snapshot):
        sel.get(url)
    
        #wait for our javascript to replace the native player with 'our' player
        time.sleep(sleep_time)
        
        #check for the "strange black box" that is mentioned in this JIRA    
        template = templateloader.load_template("test_archive_it_ari-4364.js.jinja")
        test_passed = sel.execute_script(template.render())
        
        assert test_passed == True