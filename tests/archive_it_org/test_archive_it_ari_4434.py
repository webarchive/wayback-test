import time
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

def test_ari_4434(browser_platform_choice, client_host, target, sel, build_number, snapshot_dir, test_name, sleep_time, snapshooter, templateloader, snapshot):
    
    urls = [
            {"target": target + "/4269/20150607111444/https://www.frick.org/research/photoarchive", "selector": ".flexslider-views-slideshow-main-frame-row" }, 
            {"target": target + "/4269/20150607123242/https://www.frick.org/research/photoarchive/tour", "selector": "a"},
            {"target": target + "/4269/20150607112036/https://www.frick.org/interact/education", "selector": "a"},
            {"target": target + "/4269/20150607121206/https://www.frick.org/collection/fragonard-room-lighting", "selector": "a"},
            {"target": target + "/4269/20150607121145/https://www.frick.org/collection/conservation-fifth-avenue-garden-urns", "selector": "a"},
            {"target": target + "/4269/20150607110523/https://www.frick.org/collection/education_department", "selector": "a"},
            {"target": target + "/4269/20150607121320/https://www.frick.org/collection/conservation/surveys/frame_survey", "selector": "a"},
            {"target": target + "/4269/20150607121320/https://www.frick.org/collection/conservation/surveys/enamels_survey", "selector": "a"},
            {"target": target + "/4269/20150606182505/http://www.frick.org/research/archives/highlights/historic_images", "selector": "a"}
    ]
    
    for url in urls:
        with snapshooter.prepare(sel, url, browser_platform_choice, test_name, snapshot):
            sel.get(url)
    
            template = templateloader.load_template("test_archive_it_ari_4366.js.jinja")
            sel.execute_script(template.render(selector_of_element_to_click="#dark > div > div > div.side-btn"))

            template = templateloader.load_template("test_archive_it_click_element.js.jinja")
            sel.execute_script(template.render(selector_of_element_to_click="#slat-1 > div > div > div.circle-expanded > div.nav-title"))
            
            next_picture_button = WebDriverWait(sel, 180).until(EC.presence_of_element_located((By.CSS_SELECTOR, "#section-1 > div > div.home-content > div.content > div.arrow-right > div")))
            
            assert next_picture_button != None