import time

def test_ari_4477(browser_platform_choice, client_host, target, sel, build_number, snapshot_dir, test_name, sleep_time, snapshooter, templateloader, snapshot):
    
    url = target + "/5182/20150727171302/http://www.trainwreckmovie.com/"
    
    with snapshooter.prepare(sel, url, browser_platform_choice, test_name, snapshot):
        sel.get(url)        
        
        #wait for load - this is a long loading page
        time.sleep(60)
    
        template = templateloader.load_template("test_archive_it_ari-4477-1.js.jinja")
        sel.execute_script(template.render())
        
        time.sleep(sleep_time)
        
        template = templateloader.load_template("test_archive_it_ari-4477-2.js.jinja")
        sel.execute_script(template.render())
        
        time.sleep(sleep_time)
        
        template = templateloader.load_template("test_archive_it_ari-4477-3.js.jinja")
        is_video_element_exist = sel.execute_script(template.render())
        
        assert is_video_element_exist == True