from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

def test_201703_pinterest(browser_platform_choice, client_host, target, sel, build_number, snapshot_dir, sleep_time, test_name, snapshooter, templateloader, snapshot):
    
    url = target + "/7651/20170310224520/https://www.pinterest.com/smithsonian/"
    
    with snapshooter.prepare(sel, url, browser_platform_choice, test_name, snapshot):
        sel.get(url)
    
        element = WebDriverWait(sel, 180).until(EC.presence_of_element_located((By.CSS_SELECTOR, "div._3wC7P")))
        assert element != None
