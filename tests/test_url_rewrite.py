"""
Kitchen Sink of URL-rewrite tests.
"""
import pytest
import re

@pytest.mark.parametrize("capture", [
        # https://webarchive.jira.com/browse/ARI-2775
        ('2465', '20110701145922', 'http://www.mema.state.md.us/')
        ])
@pytest.mark.wayback("ait", "global")
def test_meta_refresh_relative_url(wayback, capture):
    """Test META-REFRESH URLs are rewritten."""
    def get_meta_refresh_url(capture):
        bs = wayback.get_tree(capture)
        meta_refresh = bs.find(
            'meta',
            attrs={'http-equiv': re.compile(r'(?i)refresh')})

        content = meta_refresh['content']

        m = re.match(r'[\d.]+\s*;\s*(?i)url=(.*)', content)
        assert m, 'unexpected CONTENT: {} in {}'.format(content, meta_refresh)

        return m.group(1)

    url_orig = get_meta_refresh_url((capture[0], capture[1]+'id_', capture[2]))
    url = get_meta_refresh_url(capture)

    assert url_orig != url


@pytest.mark.wayback("ait")
def test_relative_path(wayback):
    # ARI-4167
    capture = ('4426', '20141201151358', 'http://www.mediacongo.net/home.asp')

    bs = wayback.get_tree(capture)

    # capture has a few img elements whose src starts with
    # "../docs/promo/2012/". They shall be rewritten. What's tricky here
    # is that it must be rewritten to "/docs/promo/2012/...", because
    # base URL is at the root of the server URL space and thus "../"
    # is also points to the root.
    el = bs.find('img', attrs={'src': lambda v: v.startswith('../docs/promo/')})
    assert not el

    # NB currently they are rewritten to server-relative URLs
    expected = '/{0}/{1}im_/http://www.mediacongo.net/docs/promo/'.format(
        *capture)
    el = bs.find('img', attrs={'src': lambda v: v.startswith(expected)})
    assert el

