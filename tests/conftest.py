from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from bs4 import BeautifulSoup
from jinja2 import Environment, FileSystemLoader
import pytest
import os
import re
import urllib2

valid_platforms_and_browsers = {"win7_firefox" : {"selenium_capabilities" : DesiredCapabilities.FIREFOX.copy()},
                                "win7_chrome" : {"selenium_capabilities" : DesiredCapabilities.CHROME.copy()},
                                "win7_ie11" : {"selenium_capabilities" : DesiredCapabilities.INTERNETEXPLORER.copy()},
                                "win7_ie9" : {"selenium_capabilities" : DesiredCapabilities.INTERNETEXPLORER.copy()},
                                "win8_firefox" : {"selenium_capabilities" : DesiredCapabilities.FIREFOX.copy()},
                                "win8_chrome" : {"selenium_capabilities" : DesiredCapabilities.CHROME.copy()},
                                "win8_ie10" : {"selenium_capabilities" : DesiredCapabilities.INTERNETEXPLORER.copy()},                                
                                "mac_mountain_lion_firefox" : {"selenium_capabilities" : DesiredCapabilities.FIREFOX.copy()},
                                "mac_mountain_lion_chrome" : {"selenium_capabilities" : DesiredCapabilities.CHROME.copy()},
                                "mac_mountain_lion_safari" : {"selenium_capabilities" : DesiredCapabilities.SAFARI.copy()},
                                "mac_elcapitan_firefox" : {"selenium_capabilities" : DesiredCapabilities.FIREFOX.copy()},
                                "mac_elcapitan_chrome" : {"selenium_capabilities" : DesiredCapabilities.CHROME.copy()},
                                "mac_yosemite_safari8" : {"selenium_capabilities" : DesiredCapabilities.SAFARI.copy()},
                                "mac_elcapitan_safari9" : {"selenium_capabilities" : DesiredCapabilities.SAFARI.copy()}
                                }

def pytest_addoption(parser):
    # default must be None to support auto-switching with --wayback
    parser.addoption("--target", help="Wayback host to test against",
                     action="store", default=None)
    parser.addoption("--clienthost", help="client machine where selenium rc is running",
                     action="store", default="qa-auto-vm-win1.sf.archive.org")
    parser.addoption("--browser_platform", help="platform/browser combination to use for test. Valid options are win7_chrome, win7_firefox, win7_ie11, mac_mountain_lion_chrome, mac_mountain_lion_firefox, mac_mountain_lion_safari",
                     action="append")
    parser.addoption("--build_number", help="The build number under test.",
                     action="store", default="0")
    parser.addoption("--snapshot_dir", help="The directory under which to store each builds screen shots.",
                     action="store", default="/1/ci-deploy/test_page_snapshots")
    parser.addoption("--test_name", help="The test name.",
                     action="store", default="Browser test")    
    parser.addoption("--runglobal",
                     help="run only Global Wayback tests",
                     action="store_true")
    parser.addoption("--wayback", default="ait",
                     help="symbolic name of wayback instance to test."
                     " (ait or global)")
    parser.addoption("--global", action="store_const", dest="wayback",
                     const="global", help="shortcut for --wayback global")
    parser.addoption("--sleep_time", help="The sleep time to be used in the test.",
                     action="store", default="180")
    parser.addoption("--snapshot", help="Whether to take screen shot or not.",
                     action="store_true", default=False)    
    

def pytest_runtest_setup(item):
    """
    if test is marked with ``wayback``, its argument designates wayback
    instance(s) it is meant for. It is assumed to be AIT-only if unmarked,
    and will be skipped if ``wayback`` option is other than ``ait``
    (the default). It is designed this way to allow AIT dev to add tests
    without worrying about compatibility with Global Wayback.
    """
    marks = item.keywords.get('wayback')
    # if wayback mark is not set, or it is set but has no args, assume
    # item is for AIT only.
    wbs_of_item = marks and marks.args or ["ait"]
    wb_to_test = item.config.getoption('--wayback')
    # strip off ":VARIANT" part.
    wb_to_test = re.sub(r':.*', '', wb_to_test)
    if wb_to_test not in wbs_of_item:
        pytest.skip('--wayback={0}, non-{0} tests are skipped'.format(
                wb_to_test))

@pytest.fixture
def sel(request, browser_platform):
    if not validate(browser_platform):
        print (browser_platform + " is not a valid browser_platform!")
        return
    
    browser_choice = request._funcargs["browser_platform_choice"]

    driver = webdriver.Remote(
       command_executor="http://" + client_host(request) +":4444/wd/hub",
       desired_capabilities=valid_platforms_and_browsers[browser_choice]["selenium_capabilities"])
    
    def fin():
        try:    
            print ("teardown selenium client for " + browser_choice)
            driver.close()
            driver.quit()
        except:
            print("Error when quiting selenium. May have lost connection with remote selenium server. This is NOT a test failure!")

    request.addfinalizer(fin)
    return driver;

def pytest_generate_tests(metafunc):
    #run same test for each browser_platform chosen by user
    if "browser_platform_choice" in metafunc.funcargnames:
        metafunc.parametrize("browser_platform_choice", metafunc.config.option.browser_platform)
        
def validate(browser_platform):
    """Check if all of browser_platforms are valid (listed in
    valid_platforms_and_browsers)."""
    return all(bp in valid_platforms_and_browsers
               for bp in browser_platform)
    
def sanitize_string_for_filename(str):
    return "".join(x for x in str if x.isalnum())

TARGET_DEFAULTS = {
    'ait': 'https://wayback.qa-archive-it.org',
    'ait:prod': 'https://wayback.archive-it.org',
    'global': 'http://web.archive.org',
    'global:staging': 'http://wwwb-app5.us.archive.org:8080',
    'global:py': 'http://wwwb-app5.us.archive.org:8091'
    }

@pytest.fixture
def target(request):
    value = request.config.getoption("--target")
    if value is None:
        wayback_opt = request.config.getoption("--wayback", default="ait")
        # assume "ait" if --wayback has undefined name
        value = TARGET_DEFAULTS.get(wayback_opt) or TARGET_DEFAULTS["ait"]

    # target must not end with '/'
    if value.endswith('/'):
        value = value[:-1]
    return value

class Wayback(object):
    # TODO: add move test primitives
    def __init__(self, target, variant=None):
        """Universal Wayback test agent.
        Sub-class defines specifics of different instances of Wayback
        (AIT / Global).

        :param target: root URL of wayback (see :func:`target` fixture)
        :param variant: string after ``:`` of ``--wayback`` option. such as \
        ``prod``, ``staging``.
        """
        self.target = target
        self.variant = variant
    def get_response(self, capture):
        """Get content of capture playback, return response object.

        :param capture: 3-element tuple (`collection`, `timestamp`, `URL`).
        :rtype: object :meth:`urllib2.urlopen` returns
        """
        url = self.replay_url(capture)
        resp = urllib2.urlopen(url, timeout=30)
        return resp

    def get(self, capture):
        """GET content of capture playback.

        :param capture: 3-element tuple (`collection`, `timestamp`, `URL`).
        :rtype: bytes
        """
        f = self.get_response(capture)
        try:
            return f.read()
        finally:
            f.close()

    def get_tree(self, capture):
        """Return :class:`BeautifulSoup` object populated with response.
        See `BautifulSoup documentation <http://www.crummy.com/software/BeautifulSoup/bs4/doc/>`_.

        :param capture: 3-element tuple identifying capture to load.
        """
        content = self.get(capture)
        return BeautifulSoup(content)

class ArchiveItWayback(Wayback):
    def replay_url(self, capture):
        if len(capture) == 2:
            capture = ('all', capture[0], capture[2])
        return "{0}/{1[0]}/{1[1]}/{1[2]}".format(self.target, capture)

class GlobalWayback(Wayback):
    def replay_url(self, capture):
        if len(capture) == 2:
            capture = ('web', capture[0], capture[1])
        return "{0}/web/{1[1]}/{1[2]}".format(self.target, capture)

@pytest.fixture
def wayback(request, target):
    """Returns :class:`Wayback` instance configured for Wayback instance
    identified by `target`.
    """
    wayback_opt = request.config.getoption("--wayback", default="ait")
    deployment, _, variant = wayback_opt.partition(':')
    if deployment == 'global':
        return GlobalWayback(target)
    else:
        return ArchiveItWayback(target)

@pytest.fixture
def test_name(request):
    return request.config.getoption("--test_name")

@pytest.fixture
def client_host(request):
    return request.config.getoption("--clienthost")

@pytest.fixture
def browser_platform(request):
    return request.config.getoption("--browser_platform")

@pytest.fixture
def build_number(request):
    return request.config.getoption("--build_number")

@pytest.fixture
def sleep_time(request):
    return float(request.config.getoption("--sleep_time"))

@pytest.fixture
def snapshot_dir(request):
    return request.config.getoption("--snapshot_dir")

@pytest.fixture
def snapshot(request):
    return request.config.getoption("--snapshot")

@pytest.fixture
def snapshooter(snapshot_dir, build_number, client_host):
    """Screen Snapshot service as a py.test fixture."""
    class Snapshooter(object):
        def shoot(self, webdriver, url, browser, test_name):
            d = os.path.join(snapshot_dir, str(build_number))
            if not os.path.exists(d):
                os.makedirs(d)
            snapfn = os.path.join(
                d,
                "{}_{}_{}.png".format(
                    browser,
                    client_host,
                    test_name
                    ))
            webdriver.get_screenshot_as_file(snapfn)
            
        ##must call this before with the "with"
        def prepare(self, webdriver, url, browser, test_name, snapshot):
            self.webdriver = webdriver
            self.url = url
            self.browser = browser
            self.test_name = test_name
            self.snapshot = snapshot
            return self
            
        def __enter__(self):
            return self
        
        def __exit__(self, type, value, traceback):
            try:
                if self.snapshot:    
                    self.shoot(self.webdriver, self.url, self.browser, self.test_name)
            except:
                print "Unexpected error when taking snapshot!. Continuing with rest of test!"
            
    return Snapshooter()

@pytest.fixture
def templateloader():
    class TemplateLoader(object):
        def load_template(self, filename):
            wd=os.path.dirname(os.path.abspath(__file__))
            env = Environment(loader=FileSystemLoader(wd + '/../templates'))
        
            return env.get_template(filename)
    
    return TemplateLoader()