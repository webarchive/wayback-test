"""
Test of encoded response entities (Content-Encoding, Transfer-Encoding)
"""
import pytest

@pytest.mark.wayback("global")
@pytest.mark.parametrize("capture", [
        ("http://d3n8a8pro7vhmx.cloudfront.net/assets/tinymce-jquery-b16bb09336f0e7f04e5e1d9228b0b6e8.js", "20150608032053")
        ])
def test_content_encoding_gzip(wayback, capture):
    
    resp = wayback.get_response(('web', capture[1], capture[0]))

    # original Content-Encoding header field is preserved.
    # header field name is weird currently.
    h = resp.info().getheader('X-Archive-Orig-X-Archive-Orig-Encoding')
    assert h == "gzip"

    h = resp.info().getheader("Content-Encoding")
    assert h is None

    data = resp.read(2)
    data != '\x1f\x8b'


