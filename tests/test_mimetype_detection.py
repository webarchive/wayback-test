"""
Test cases for mime-type detection.
"""
import pytest
import re
import socket
import urllib2

@pytest.mark.parametrize("capture", [
        # mixed case DocType
        (None, '20010717141117', 'http://www.0-1.ac/esoteric/cat/kit/index.html'),
        # mixed case tag names
        (None, '20010808210429', 'http://www.0-1.ac/esoteric/cat/nutrient/index.html'),
        (None, '20140517111442', 'http://0002.ac/'),
        # has DOCTYPE, no <HTML> but has <HEAD>
        (None, '20040606110138', 'http://print.11.ac/'),
        # regular XHTML
        (None, '20130731105846', 'http://setrum.120v.ac/'),
        # long HTML comment at the beginning
        (None, '20040601072056', 'http://www.21edu.ac/tt/board/ttboard.cgi?db=21edu_board1&page=1'),
        # multiple HTML comments at the beginning
        (None, '20140825204123', 'http://www.asahi.ac/shop/ekikita.html'),
        # <html lang="ja">
        # this capture has gone missing - find another
        #(None, '20030108193124', 'http://www.21th.ac:80/cgi-bin/abicorp/kboard.cgi?mode=res_html&owner=%82%A4%82%A6&epass='),
        # starts with <table ...
        (None, '20020610020938', 'http://www.21th.ac/cgi-bin/chat2/abibartp.cgi?window=0&reload=0&mode=checked'),
        ])
@pytest.mark.wayback('global')
def test_html_toolbar_insert(wayback, capture):
    """HTML samples. Check if Navigation Toolbar is inserted."""
    content = wayback.get(capture)
    assert re.search(r'BEGIN WAYBACK TOOLBAR INSERT', content)

@pytest.mark.parametrize("capture", [
        # ACC-48
        ('congress110th-test', '20081013222444', 'http://www.cole.house.gov/newsroom/getArticle.aspx?Type=Press%20Release&Date=1&Length=240'),
        # starts with ";window.Modernizer=function(a,b,c){"
        ('5182', '20150220172902js_', 'http://media.fiftyshadesofgreymovie.com/js/Main.min.js')
        ])
@pytest.mark.wayback('global')
def test_javascript(wayback, capture):
    """Javascript rewritten as an HTML. Javascript without expected
    patterns and with document.write's are easily mistaken to be an HTML."""

    content = wayback.get(capture)

    # XXX - this depends on particular rendering template; may not work
    # for all deployments.
    assert not re.search(r'BEGIN WAYBACK TOOLBAR INSERT', content)

@pytest.mark.parametrize("capture", [
        # JSON with Content-Type: text/html; charset=UTF-8
        (None, '20120329074648', 'http://www.101domain.ac/includes/form.php'),
        ])
@pytest.mark.wayback('global')
def test_javascript(wayback, capture):
    """JSON."""
    content = wayback.get(capture)

    # XXX - this depends on particular rendering template; may not work
    # for all deployments.
    assert not re.search(r'BEGIN WAYBACK TOOLBAR INSERT', content)


@pytest.mark.parametrize("capture", [
        ('4544', '20140626220913cs_', 'http://wangechimutu.com/wp-content/themes/wangechi_mutu/images/splotch_alpha/splotch_art_alpha0.png')
        ])
@pytest.mark.wayback('ait')
def test_mimetype_sniffing_over_context_flag(wayback, capture):
    # despite "cs_" flag, detected content-type takes precedence.
    content = wayback.get(capture)
    assert content.startswith('\x89PNG')

@pytest.mark.parametrize("capture", [
        # cpatures whose replay once fell into busy loop for hours,
        # due to a loose regular expression in SimpleMimeTypeDetector.
        # Note: some of these are captures of 404 response, which Wayback
        # replays as 404.
        (None, '20120405141537im_', 'http://www.independent.co.uk/multimedia/archive/00250/pg-22-cot-death-EDP_250574m.jpg'),
        (None, '20111111081742im_', 'http://www.independent.co.uk/multimedia/archive/00121/stonehenge_thumb_121882a.jpg'),
        (None, '20061017072550', 'http://www.purpletribute.com/'),
        (None, '20140104014027', 'http://tindoanhnghiep.com/'),
        (None, '20140909052721', 'http://www.ottawacitizen.com/'),
        (None, '20090211131529', 'http://www.active.com/event_detail.cfm?event_id=1649810'),
        (None, '20140103114455', 'http://livcouture.com/'),
        (None, '20120405141536im_', 'http://www.independent.co.uk/multimedia/archive/00250/pg-7-gaga-EDPIC_250500m.jpg'),
        (None, '20141201115823im_', 'https://seal.globalsign.com/SiteSeal/images/gmogs_image_125-50_en_white.png'),
        (None, '20141203203055', 'http://sport.bt.com/ajaxapic?en=video/getVideo'),
        (None, '20120405141535im_', 'http://www.independent.co.uk/multimedia/archive/00250/Pg-14-ballet-6666_250845m.jpg'),
        (None, '20140105114048', 'http://thekinghasnoclothes.com/'),
        (None, '20041127141735', 'http://www.guardamar-information.co.uk/'),
        (None, '20090303133351', 'http://www.freepeople.com/index.cfm/fuseaction/category.content/categoryID/6173cf1d-54d5-4e16-8cf3-98ded488ba58'),
        (None, '20120320192218', 'http://www.independent.co.uk/multimedia/archive/00096/EDhonda_96462m.jpg"'),
        (None, '20120405141535im_', 'http://www.independent.co.uk/multimedia/archive/00250/balls_getty66x66_250758m.jpg'),
        (None, '20091026234920', 'http://www.esipc.sa.gov.au/'),
        (None, '20121031222942', 'http://msft.digitalrivercontent.net/office2010/X16-32250.exe'),
        (None, '20140712120202', 'http://www.shareasale.com/shareasale.cfm?merchantID=49497'),
        (None, '20081001111507', 'http://www.politico.com/news/stories/0908/13602.html'),
        (None, '20141012211659', 'https://www.charterbusiness.com/'),
        (None, '20111128003350im_', 'http://www.independent.co.uk/multimedia/archive/00033/IN4721195October-200_33052t.jpg')
        ])
@pytest.mark.wayback('global')
def test_runaway_regexp(wayback, capture):
    timeout = False
    try:
        content = wayback.get(capture)
    except urllib2.HTTPError as ex:
        # replayed 404 capture
        pass
    except socket.error as ex:
        timeout = True

    assert not timeout, capture
