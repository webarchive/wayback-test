"""
Test WWM-142 `https://webarchive.jira.com/browse/WWM-142`_,
XHTML compliance. Banner / toolbar HTML must be XHTML-parsable
at least.

This test does not check XHTML validity currently.
"""
import sys
import re
import urllib2

import xml.etree.ElementTree as ET

import pytest

# https://webarchive.jira.com/browse/WWM-142
CAPTURE = ('20140420185608', 'http://studyinghttp.net/rfc_ja/rfc2616')

@pytest.mark.wayback('global')
def test_xhtml_renders_ok(wayback):
    content = wayback.get(CAPTURE)
    # The original resource is in EUC-JP encoding, but Wayback re-encode it
    # in UTF-8. As expat library fails on encoding="EUC-JP" in XML decl,
    # rewrite it to UTF-8.
    content = re.sub(r'encoding="EUC-JP"', 'encoding="UTF-8"', content, 1)
    # it also has '&copy;' - replace it with '&#169;'
    content = re.sub(r'&copy;', '&#169;', content)

    xml = ET.XML(content)

