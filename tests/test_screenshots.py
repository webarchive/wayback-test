"""
Replay of screeen captures.
PNG files are saved as WARC metadata records.
"""
import pytest

@pytest.mark.wayback('global')
@pytest.mark.parametrize("capture", [
        ('http://yahoo.zonaprop.com.ar/', '20120111143309'),
        ('http://yahoo.co.jp/', '20120824184811'),
        ('http://yahoo.com/', '20130127173823'),
        ('http:/a-1-alloys.allitwares.com', '20130313024055')
        ])
def test_screenshots(wayback, capture):
    c = wayback.get(('web', capture[1],
                     'http://web.archive.org/screenshot/' + capture[0]))
    # screenshots are saved as PNG
    c[:4] == '\x89PNG'
